# SIB-env

Environment for SIB training

## Schema

Made with [ASCIIFlow](http://asciiflow.com)

```
    Student computer                Virtual machines

    +-------------+               +------------------+
    |  Ansible    |               |      CentOS      |
    | controller  |  10.42.0.0/24 |   +----------+   |
    |             +---------------+   | node1-c7 |   |
    | 10.42.0.10  |      SSH      |   |          |   |
    |             |               |   |10.42.0.11|   |
    +-------------+               |   |          |   |
                                  |   |SSH port :|   |
      Working dirs                |   |   2211   |   +-----------------------+
                                  |   +----------+   |                       |
+------------------------+        |                  |     user: vagrant     |
| Vagrant: $HOME/vagrant |        |      Debian      |    with sudo rights   |
| Ansible: $HOME/SIB     |        |   +----------+   |                       |
+------------------------+        |   | node2-deb|   +-----------------------+
                                  |   |          |   |
     Machine access               |   |10.42.0.12|   |
                                  |   |          |   |
+------------------------+        |   |SSH port :|   |
|        ssh node1       |        |   |   2212   |   |
|        ssh node2       |        |   +----------+   |
+------------------------+        |                  |
                                  +------------------+

```

## Student usage

``` shell
mkdir $HOME/vagrant
cd $HOME/vagrant
git clone https://gitlab.com/azyx/sib-env.git
cd sib-env
vagrant up
```
You will have two VM: one with CentOS7 and another with Debian Stretch running on your
machine based on [inventory file](inventory/hosts).


## Facilitator usage
